package android.ramirezh.crystalball20;

public class Predictions {

    private  static  Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[] {
            "Your wishes will come true.",
            "YOUR WISHES WILL NEVER COME TRUE!!!"
        };
    }

    public static Predictions get() {
        if (predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPrediction() {

        return answers[1];
    }
}
